<?php

namespace AppVentus\ArchimedeBundle\Builders;

use Doctrine\Common\Collections\ArrayCollection;


class FiltersBuilder
{



    private $entityMetadatas;
    private $builder;

    public function __construct($em = null,$currentFilterValues = array())
    {
        $this->em = $em;
        $this->currentFilterValues = $currentFilterValues;
    }

    public function setEntityMetadatas($entityMetadatas)
    {
        $this->entityMetadatas = $entityMetadatas;
    }
    public function setBuilder($formFactory)
    {
        $this->builder = $formFactory->createBuilder('form');
    }
    public function getBuilder()
    {
        return $this->builder;
    }

    public function setCurrentFilterValues($currentFilterValues)
    {
        $this->currentFilterValues = $currentFilterValues;
    }


    public function buildFilters($entityName, $criterias, $scope = null, $operandes = null, $options = array(), $render = "" ){

        //get entity metadatas to detect type of criterias
        $this->setEntityMetadatas($this->em->getMetadataFactory()->getMetadataFor($entityName));

        foreach($criterias as $key => $criteria){
            $option = array();
            if(!is_array($criteria)){
                if(array_key_exists($criteria, $options))
                    $option = $options[$criteria];
        }else{
                $option = $options;
           }
            $type = $this->defineCriteriaType($criteria, $option);
            if($type == 'aggregate'){
                //if criteria is aggregate, store his scope

                $fields_names = array_keys($criteria);
                $metadatas[$fields_names[0]] = array('type'=>$type, 'options'=>$option,'scope'=>$criteria[$fields_names[0]]);
            }elseif($type == 'api'){
                //add each api criteria
                foreach($criteria['api'] as $criteriaItem){
                    $metadatas[$criteriaItem['entityField']] = array('type'=>$type);
                }
            }else{
                $metadatas[$criteria] = array('type'=>$type, 'options'=>$option);

            }
        }
        //store request scope in metadatas (scope is a static criteria invisible for user, like published status for ad entity or available for user entity)
        if(!is_null($scope))
        $metadatas['scope']= $scope;
        //store request SQL operandes (=, >, <, !=, LIKE, ...) for each criteria
        if(!is_null($operandes))
        $metadatas['operandes']= $operandes;

        if(array_key_exists("joins", $options)){
            foreach($options["joins"] as $join){
                foreach($join as $key=>$joinItem){
                    $metadatas['joins'][$key]= $joinItem;
                }
            }
        }
        if(array_key_exists("leftjoins", $options)){
            foreach($options["leftjoins"] as $join){
                foreach($join as $key=>$joinItem){
                    $metadatas['leftjoins'][$key]= $joinItem;
                }
            }
        }
        //store twig render view, you can pass you custom view or use the default one
        $metadatas['render']= $render;
        foreach($options as $optionKey => $optionValue){
            if(!isset($metadatas[$optionKey])){
                $metadatas[$optionKey] = $optionValue;
            }
        }

    $form = $this->getBuilder()->getForm();
// print_r($criterias);exit;
        return array("entity"=>$entityName,"metadatas"=>json_encode($metadatas));
    }
    //return the type of the criteria, for aggregate search, can recurse himself to check if given values are correct
	public function guessCriteriaType($criteria, $option = array()){
        if(array_key_exists(0, $option)){
        if(array_key_exists('type', $option[0])){
            return $option[0]['type'];
        }
    }
        //if criteria is an array and sub criterias are valid, it's an aggregation
         if(is_array($criteria)){
            if(array_key_exists('api', $criteria)){
                return "api";
            }
                $recursion = array();
                    foreach($criteria as $criteriaItem){
                        foreach($criteriaItem as $criteriaSubItem){
                            if(array_key_exists($criteriaSubItem,$option))
                            $recursion[] = $this->guessCriteriaType($criteriaSubItem, $option[$criteriaSubItem]);
                        }
                    }
    	 	// $recursion = array_map(array("AppVentus\ArchimedeBundle\Builders\FiltersBuilder", "guessCriteriaType"),$criteria,$option[$criteria]);
    	 	if(!in_array(false, $recursion) && $recursion != array()){
    	 		return "aggregate";
    	 	}else{
    	 		return false;
    	 	}
    	 }

            //check if criteria is in association mapping of the entity
			if(array_key_exists($criteria, $this->entityMetadatas->associationMappings)){
				return "relation";
            //check if criteria is in field mapping of the entity
			}else if(array_key_exists($criteria, $this->entityMetadatas->fieldMappings)){
				return "field";
			}else{
				return false;
			}
	}
    //add entity field type to the search form
    protected function builderAddCustomRelation($name, $entity){
        $data = '';
            //check if value is currently selected for this form field and add it to default data
            if(array_key_exists($name, $this->currentFilterValues)){
                $data = $this->currentFilterValues[$name];
                //$this->builder->setData(array($name=>$this->currentFilterValues[$name]));
            }
            $this->builder->add($name, 'text', array(
                'required' => false,
                'data' => $data
             ));
    }

    //add entity field type to the search form
protected function builderAddEntity($name, $entity,$option){
        if(is_array($option) && array_key_exists(0, $option) && array_key_exists('values', $option[0])){

        // print_r($option[0]['multiple']);exit;
            $this->builderAddChoice($name, $option);
        }else{
		$collection = new ArrayCollection();
            //if values are currently selected, add the as default values (data) to the form field
			if(array_key_exists($name, $this->currentFilterValues)){
                foreach($this->currentFilterValues[$name] as $entityId){
                    $entityObj = $this->em->getRepository($entity)->find($entityId);
                    $collection->add($entityObj);

                }
            }

			$this->builder->add($name, 'entity', array(
			    'class' => $entity,
			    'multiple'  => true,
			    'required' => false,
			    'data' => $collection
			));
        }
}
    //add a text field to the form
	protected function builderAddField($name){
		$data = '';
            //check if value is currently selected for this form field and add it to default data
			if(array_key_exists($name, $this->currentFilterValues)){
				$data = $this->currentFilterValues[$name];
				//$this->builder->setData(array($name=>$this->currentFilterValues[$name]));
			}
			$this->builder->add($name, 'text', array(
			    'required' => false,
			    'data' => $data
			 ));
	}//add a choice to the form
    protected function builderAddChoice($name, $option){
        $data = array();
            if(!array_key_exists('multiple', $option[0])){
                $option[0]['multiple'] = true;
            }
            //check if value is currently selected for this form field and add it to default data
            if(array_key_exists($name, $this->currentFilterValues)){
                $data = $this->currentFilterValues[$name];
                //$this->builder->setData(array($name=>$this->currentFilterValues[$name]));
            }elseif($option[0]['multiple'] == false){
                $data = null;
            }
        // print_r($option[0]['multiple']);exit;
            $this->builder->add($name, 'choice', array(
                'required' => false,
                'multiple' => $option[0]['multiple'],
                'choices' => $option[0]['values'],
                'data' => $data,
                'empty_value'=> 'Peu importe',
                'empty_data'  => null
             ));
    }
    //build form field for the selected criteria
	public function defineCriteriaType($criteria, $option = array()){

    	 switch ($this->guessCriteriaType($criteria, $option)) {
    	 	case 'field':
			$this->builderAddField($criteria);
			return 'field';
    	 		break;
    	 	case 'relation':
			$this->builderAddEntity($criteria, $this->entityMetadatas->associationMappings[$criteria]['targetEntity'], $option);
			return 'relation';
    	 		break;
    	 	case 'api':
                foreach($criteria['api'] as $criteriaItem){
                    $this->defineCriteriaType($criteriaItem['entityField']);
                }
			     return 'api';
    	 	 break;
            case 'aggregate':
            $fields_names = array_keys($criteria);
            $this->builderAddField($fields_names[0]);
            return 'aggregate';
                break;
            case 'choice':
            $this->builderAddChoice($criteria, $option);
            return 'choice';
            case 'customRelation':
            $this->builderAddCustomRelation($criteria, $option);
            return 'customRelation';
                break;
    	 	//if none field type reconized, throw excemtion with intelligent message
    	 	default:
            if(is_array($criteria)){
            $fields_names = array_keys($criteria);
    	 	$message = is_array($criteria)?implode(" or ", $criteria[$fields_names[0]]):$criteria;
            }else{
                $message = $criteria;
            }
    			throw new \Exception('Chosen parameter "'.$message.'" is invalid.');
    	 		break;
    	 }
		return true;
	}

}
