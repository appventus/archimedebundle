<?php

namespace AppVentus\ArchimedeBundle\Builders;

use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;


class PaginatorBuilder
{

    private $queryBuilder;
    private $em;

    public function __construct($em)
    {
        $this->em = $em;
        $this->queryBuilder = new QueryBuilder($em);
    }


        //build the query with given values
    public function create($taintedValues, $page = 1, $maxPerPage = 10)
    {
        $query = $this->queryBuilder->searchQuery($taintedValues);
        $adapter = new DoctrineORMAdapter($query);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($maxPerPage)
                   ->setCurrentPage($page);
                
        return $pagerfanta;
    }

}