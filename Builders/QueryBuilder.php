<?php

namespace AppVentus\ArchimedeBundle\Builders;

use Symfony\Component\HttpKernel\Exception\HttpException;



class QueryBuilder
{

    private $filtersBuilder;
    private $em;
    private $container;

    public function __construct($em, $container = null,$apiManager = null, $filtersBuilder = null)
    {
        $this->em = $em;
        $this->container = $container;
        $this->apiManager = $apiManager;
        if(!$filtersBuilder)
            $filtersBuilder = new FiltersBuilder($em);

        $this->filtersBuilder = $filtersBuilder;
    }

    public function search($page, $way, $orderBy, $maxPerPage)
    {
        //compare given search values and session stored values
        if ('POST' === $this->container->get('request')->getMethod()) {
            $all_values = $this->container->get('request')->request->all();
            $formValues = $all_values['form'];
            $this->container->get('session')->set("filter_values",$formValues);
            $metas = get_object_vars(json_decode($formValues['metadatas']));
            $entity = $formValues['entity'];
        }else{
            $sessionValues = $this->container->get('session')->get("filter_values", array());
            // var_dump($sessionValues);exit;
            $this->container->get('session')->set("filter_values",$sessionValues);
            $metas = get_object_vars(json_decode($sessionValues['metadatas']));
            $entity = $sessionValues['entity'];
        }
        $render = get_object_vars($metas['render']);
        $this->render = $render['list'];
        //get entity metadatas to detect type of criterias
        $this->filtersBuilder->setEntityMetadatas($this->container->get('doctrine')->getManager()->getMetadataFactory()->getMetadataFor($entity));
        // $query = $this->searchQuery($this->container->get('session')->get("filter_values"));

        //paginate results
        $paginator = $this->container->get('search_engine.paginator')->create($this->container->get('session')->get("filter_values"), $page, $maxPerPage);

        return array(
            "page"=>$page,
            "way"=>$way,
            "orderBy"=>$orderBy,
            "paginator"=>$paginator,
            );

    }

        //build the query with given values
    public function searchQuery($taintedValues, $debugMode = false)
    {

            $this->entity = $taintedValues['entity'];
        //decode metadatas from json, and convert stdClasses to simple array values
        $metadatas = array();
        if(array_key_exists('metadatas', $taintedValues)){
            $metadatas = json_decode($taintedValues['metadatas'], true);
                // if(is_object($metadatas))
                //     $metadatas = array_map( "get_object_vars",get_object_vars($metadatas));
        }
        //get entity metadatas
            $this->filtersBuilder->setEntityMetadatas($this->em->getMetadataFactory()->getMetadataFor($this->entity));
            //if search scope is given, get it
        $scope = array_key_exists('scope', $metadatas)?$metadatas['scope']:array();
        //if custom operandes are given for the request, get it
            $operandes = array_key_exists('operandes', $metadatas)?$metadatas['operandes']:array();
        //if custom orderBy are given for the request, get it
        $orderBy = array_key_exists('orderBy', $metadatas)?$metadatas['orderBy']:null;
        //if custom orderBy are given for the request, get it
        $joins = array_key_exists('joins', $metadatas)?$metadatas['joins']:array();
        $leftjoins = array_key_exists('leftjoins', $metadatas)?$metadatas['leftjoins']:array();
        $lastjoins = array_key_exists('lastjoins', $metadatas)?$metadatas['lastjoins']:array();
        $lastleftjoins = array_key_exists('lastleftjoins', $metadatas)?$metadatas['lastleftjoins']:array();
        //clean metadatas and tainted values arrays, keep only fields to search in
        unset($metadatas['render']);
            unset($metadatas['scope']);
            unset($metadatas['operandes']);

            unset($taintedValues['_token']);
            unset($taintedValues['entity']);
            unset($taintedValues['metadatas']);


             $this->queryParameters = array();
         $select = (!$joins == array())?", ".implode(", ", array_flip($joins)):"";
         $select .= (!$leftjoins == array())?", ".implode(", ", array_flip($leftjoins)):"";
         $select .= (!$lastjoins == array())?", ".implode(", ", array_flip($lastjoins)):"";
         $select .= (!$lastleftjoins == array())?", ".implode(", ", array_flip($lastleftjoins)):"";
         //We setup the query structure. start with the select part, and we'll define joins and conditions.
         $this->queryHeader = 'SELECT entity '.$select.' FROM '.$this->entity.' entity ';
         $this->queryJoins =array();
         $this->queryConditions=array();
         $this->queryOrderBy=array();
        $this->apiXMLQueryPart = array();
        $this->apiXMLMetadataPart = array();
        $isApiSearch = false;
            foreach($taintedValues as $criteria => $taintedValue){
            if(array_key_exists($criteria, $metadatas)){
                $criteriaType = $metadatas[$criteria]['type'];
            }else{
                $criteriaType = $this->filtersBuilder->guessCriteriaType($criteria);
            }
        //get operande for current criteria
            $operande = array_key_exists($criteria, $operandes)?$operandes[$criteria]:"=";
                    // if(array_key_exists($criteria, $metadatas)){
                if($criteriaType == 'api'){
                    $isApiSearch = true;
                    $this->addApiXMLPart($criteria, $taintedValue);

                }else{
                //call the right function to build the query depending of the type (aggregate, field or relation) of the criteria
                    if($criteriaType == ""){
                        $criteria = is_array($criteria)?explode($criteria,', '):$criteria;
                        throw new HttpException(500, "Error, the criteria '".$criteria."' has no type defined, maybe you forget to set something like metadatas['".$criteria."']['type'] value");

                    }
                        $this->{"addQuery".ucfirst($criteriaType)."Search"}($criteria, $taintedValue, $operande, array_key_exists($criteria, $metadatas)?$metadatas[$criteria]:null);
                }
               // }
            }
        if($isApiSearch){
            $xml = $this->buildApiXML();
            $apiUsers = $this->apiManager->searchCandidates($xml);
            $this->addQueryApiSearch($apiUsers);

        }
            $this->addQueryScope($scope, $operandes, $metadatas);

        if(!is_array($orderBy)){
            $orderBy = array();
            $this->queryOrderBy = array("entity.id DESC");
        }
        foreach($joins as $alias => $join){
            array_push($this->queryJoins, "JOIN ".$join." ".$alias);
        }
        foreach($leftjoins as $alias => $leftjoin){
            array_push($this->queryJoins, "LEFT JOIN ".$leftjoin." ".$alias);
        }
        foreach($lastjoins as $alias => $join){
            array_push($this->queryJoins, "JOIN ".$join." ".$alias);
        }
        foreach($lastleftjoins as $alias => $leftjoin){
            array_push($this->queryJoins, "LEFT JOIN ".$leftjoin." ".$alias);
        }

        foreach($orderBy as $orderByValue => $orderByWay){
            if(is_array($orderByWay)){
                foreach($orderByWay as $orderByValueKey => $orderByWayItem){
                    $orderByValueKey = strstr($orderByValueKey, ".")?$orderByValueKey:"entity.".$orderByValueKey;
                    array_push($this->queryOrderBy, $orderByValueKey." ".$orderByWayItem);
                }
            }else{
            $orderByValue = strstr($orderByValue, ".")?$orderByValue:"entity.".$orderByValue;
            array_push($this->queryOrderBy, $orderByValue." ".$orderByWay);

            }
            //if the orderBy given field doesnt contain a dot, add "entity." before it
        }
        $this->queryOrderBy = ($this->queryOrderBy != array())?"ORDER BY ".implode(",", $this->queryOrderBy):"";
        $this->queryConditions = ($this->queryConditions != array())?"WHERE ".implode(" AND ", $this->queryConditions):"";
        //build the query string, with select part first, then joins and conditions
            $query = $this->queryHeader." ".implode(" ",$this->queryJoins)." ".$this->queryConditions." ". $this->queryOrderBy;
            // print_r($query);
     //    print_r($this->queryParameters);exit;

                $query = $this->em->createQuery($query);
            if($debugMode){
                return array('SQL'=>$query->getSql(), 'parameters'=>$this->queryParameters);
            }
                //add the condition parameters to the query object
                foreach($this->queryParameters as $key=>$param){
                        $query->setParameter($key+1, $param);
                }
        //return query object
         return $query;

    }



    protected function addQueryScope($scope, $operandes, $metadatas){
            foreach($scope as $field => $scopeItem){
            //check if custom operand is provided, else give default =
            $operande = array_key_exists($field, $operandes)?$operandes[$field]:'=';
        //add query part corresponding to criteria
            $this->{"addQuery".ucfirst($this->filtersBuilder->guessCriteriaType($field))."Search"}($field, $scopeItem[0], $operande);


            };


    }

    //add relation search query part
    protected function addQueryChoiceSearch($criteria, $values, $operande = "=", $metadatas = null){
        if($values != array() && $values != ''){
            //add join part
            // array_push($this->queryJoins,"JOIN entity.".$criteria." ".$criteria);
            $subconditions = array();
            //for each selected value
            if(is_array($values)){
                foreach($values as $key =>$value){
                    //find correct parameter number
                    $parameterNumber = sizeof($this->queryParameters)+1;
                    //add query part string with criteria and operande
                    array_push($subconditions, "entity.".$criteria." ".$operande." (?".$parameterNumber.")");
                    //add value searched with correct synthax
                    array_push($this->queryParameters, $this->operize($value, $operande));
                }
                //transform all subconditions array in string separed by OR and put them in the condition query part
                array_push($this->queryConditions, "(".implode(" OR ", $subconditions).")");
            }else{
                    $parameterNumber = sizeof($this->queryParameters)+1;
                    //add query part string with criteria and operande
                    array_push($this->queryConditions, "entity.".$criteria." ".$operande." ?".$parameterNumber);
                    //add value searched with correct synthax
                    array_push($this->queryParameters, $this->operize($values, $operande));

            }
        }
    }
    //add field search query part
    protected function addQueryFieldSearch($criteria, $value, $operande = "=", $metadatas = null){
        if ($value != "") {
            $operande = $operande?$operande:"=";
            //find correct parameter number
            $parameterNumber = sizeof($this->queryParameters)+1;
            //add query part string with criteria and operande
            array_push($this->queryConditions, "entity.".$criteria." ".$operande." ?".$parameterNumber);
            //add value searched with correct synthax
            array_push($this->queryParameters, $this->operize($value, $operande));
        }

    }
    //add field search query part
    protected function addQueryJoinSearch($criteria, $value, $operande = "=", $metadatas = null, $field = 'id'){
        if($value != ""){
            $operande = $operande?$operande:"=";
            //find correct parameter number
            $parameterNumber = sizeof($this->queryParameters)+1;
            //add query part string with criteria and operande
            array_push($this->queryConditions, $criteria.".".$field." ".$operande." (?".$parameterNumber.")");
            //add value searched with correct synthax
            array_push($this->queryParameters, $this->operize($value, $operande));
        }

    }
    //add relation search query part
    protected function addQueryRelationSearch($criteria, $values, $operande = "=", $metadatas = null){
        if($values != array()){
            $operande = $operande?$operande:"=";
            //add join part
                array_push($this->queryJoins,"LEFT JOIN entity.".$criteria." ".$criteria);
                $subconditions = array();
            //for each selected value

            if(is_array($values)){
                        foreach($values as $key =>$value){
                        //find correct parameter number
                        $parameterNumber = sizeof($this->queryParameters)+1;
                        //add query part string with criteria and operande
                        array_push($subconditions, $criteria.".id ".$operande." (?".$parameterNumber.")");
                        //add value searched with correct synthax
                        array_push($this->queryParameters, $this->operize($value, $operande));
                        }
                //transform all subconditions array in string separed by OR and put them in the condition query part
                array_push($this->queryConditions, "(".implode(" OR ", $subconditions).")");
                }else{
                    if($values != ''){
                        //find correct parameter number
                        $parameterNumber = sizeof($this->queryParameters)+1;
                        array_push($this->queryParameters, $this->operize($values, $operande));
                        array_push($this->queryConditions, $criteria.".id ".$operande." ?".$parameterNumber);
                    }
                }
        }
    }
    //add relation search query part
    protected function addQueryCustomRelationSearch($criteria, $values, $operande = "=", $metadatas = null){
        $options = $metadatas['options'][0];

        if ($values != array()) {
            $operande = $operande?$operande:"=";
            if(array_key_exists('field', $options)){
                $criteria = $options['field'];
            }
                //find correct parameter number
                $parameterNumber = sizeof($this->queryParameters)+1;

                array_push($this->queryConditions, $options['entity'].".".$criteria." ".$operande." (?".$parameterNumber.")");

                //add value searched with correct synthax
                array_push($this->queryParameters, $this->operize($values, $operande));

        }
    }
    //add aggregate search query part
    protected function addQueryAggregateSearch($criterias, $value, $operande = "=", $metadatas = null){
        if($value != array() && $value != ""){
                $subconditions = array();
                //for each item to search in
                foreach($metadatas['scope'] as $key => $scope){
                //separate words given in search field to search each of them
                foreach(explode(" ", $value) as $valueItem){
                    //find correct parameter number
                        $parameterNumber = sizeof($this->queryParameters)+1;
                                        if(array_key_exists("options", $metadatas) && array_key_exists($scope, $metadatas["options"]) && $metadatas["options"][$scope][0]["type"] != "field"){
                                            $this->{"addQuery".ucfirst($metadatas["options"][$scope][0]["type"])."Search"}($scope, $valueItem, $operande[$key],array("options"=>$metadatas["options"][$scope]));

                                            array_push($subconditions, array_pop($this->queryConditions));
                                            array_push($this->queryParameters, array_pop($this->queryParameters));

                                        }else{

                //add query part string with criteria and operande
                        array_push($subconditions, "entity.".$scope." ".$operande[$key]." ?".$parameterNumber);
                //add value searched with correct synthax
                        array_push($this->queryParameters, $this->operize($valueItem, $operande[$key]));
                                        }
                }
        }
            //transform all subconditions array in string separed by OR and put them in the condition query part
            array_push($this->queryConditions, "(".implode(" OR ", $subconditions).")");
            }

    }
    //check value of operand and correct value synthax
    protected function operize($value, $operande){

            switch ($operande) {
            case 'LIKE':
                return "%".$value."%";
                break;
            case null:
                return "=";
                break;

        default:
        return $value;
        break;
            }
    }


    //add field search query part
    protected function addQueryApiSearch($apiUsers){
        $dybUsersIds = array();
        if(is_array($apiUsers['results'])){
            foreach($apiUsers['results'] as $user){
                array_push($dybUsersIds, $user['result'][0]['resume'][0]['userId']);
            }
        }else{
            $dybUsersIds = array(null);
        }
            //find correct parameter number
            $parameterNumber = sizeof($this->queryParameters)+1;
            //add query part string with criteria and operande
            array_push($this->queryConditions, "entity.dybId IN (?".$parameterNumber.')');
            //add value searched with correct synthax
            array_push($this->queryParameters, $dybUsersIds);


    }

    protected function searchByDYBAPI($criterias){

        $request = '<search>';
            foreach($criterias as $criteria){
            $request .= '
            <metadatas>';
                    $request .= '
                            <metadata>trades:'.$trade.'</metadata>
                        ';
    $request .= '
            </metadatas>';

            }
        $request .= '
        </search>';


    }

    protected function buildApiXML(){
        $xml = '<search>';
        $xml .= '<queries>'.implode("", $this->apiXMLQueryPart).'</queries>';
        $xml .= '<metadatas>'.implode("", $this->apiXMLMetadataPart).'</metadatas>';
        $xml .= '</search>';
        return $xml;
    }
    protected function addApiXMLPart($criteria, $value){
        switch ($criteria) {
            case 'trades':
                $this->addApiXMLMetadata($criteria,$value);
                break;

            default:
                $this->addApiXMLQuery($criteria,$value);
                break;
        }
    }
    protected function addApiXMLMetadata($criteria, $value){
        $part = '';
        foreach($value as $valueItem){
            $part .= '<metadata>'.$criteria.':'.$valueItem.'</metadata> ';
        }
        array_push($this->apiXMLMetadataPart, $part);
    }
    protected function addApiXMLQuery($criteria, $value){
        if($value != ""){
            $part =
            '<query>
                <term>'.$value.'</term>
                <fields>
                    <in>cv</in>
                    <in>jobs</in>
                    <in>educations</in>
                    <in>skills</in>
                    <in>location</in>
                </fields>
            </query> ';
            array_push($this->apiXMLQueryPart, $part);
        }
    }

    public function getSql($values){
        return $this->searchQuery($values, true);
    }

    /**
     *@Return EntityManager
     */
    public function getEntityManager(){
        return $this->em;
    }
}
