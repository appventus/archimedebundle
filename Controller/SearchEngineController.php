<?php

namespace AppVentus\ArchimedeBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Doctrine\Common\Collections\ArrayCollection;

/**
*
 * @Route(service="search_engine_service")
*/
class SearchEngineController extends ContainerAware
{

    public function preExecute(){
        //get session's stored search values
        $this->currentFilterValues = $this->container->get('session')->get("filter_values", array());
        $this->filtersBuilder = $this->container->get('search_engine.filter');
        $this->filtersBuilder->setCurrentFilterValues($this->currentFilterValues);
        $this->queryBuilder = new \AppVentus\ArchimedeBundle\Builders\QueryBuilder($this->container->get('doctrine')->getManager(),null,$this->filtersBuilder);

    }
    /**
     * @Template()
     * Called by the twig renderer, display a search form.
     */
    public function filtersAction($entityName, $criterias, $scope = null, $operandes = null, $options = array(), $render = "" , $ajax = false)
    {
        $this->preExecute();
        $em = $this->container->get('doctrine')->getManager();

        $metadatas= $this->filtersBuilder->buildFilters($entityName, $criterias, $scope, $operandes, $options, $render );
        $form = $this->filtersBuilder->getBuilder()->getForm();

    return $this->container->get('templating')->renderResponse($render['filters'].'.html.twig', array('form' => $form->createView(), 'entity' => $entityName, 'ajax' => $ajax, 'metadatas' => $metadatas['metadatas']));
    }
	/**
     * @Route("/recherche/{page}/{way}/{orderBy}", defaults={"page" = "1", "way" = "ASC", "orderBy" = "title"}, name="searchengine_search")
     * @Template()
     */
    public function searchAction($page, $way, $orderBy)
    {
        $params = $this->search($page, $way, $orderBy);
        if($this->container->get('request')->isXmlHttpRequest()){
        return $this->container->get('templating')->renderResponse($this->render.'Partial.html.twig', $params);
        }else{
    	return $this->container->get('templating')->renderResponse($this->render.'.html.twig', $params);
     }
    }

    public function search($page, $way, $orderBy)
    {
        $this->preExecute();
        //compare given search values and session stored values
        if ('POST' === $this->container->get('request')->getMethod()) {
            $all_values = $this->container->get('request')->request->all();
            $formValues = $all_values['form'];
            $this->container->get('session')->set("filter_values",$formValues);
            $metas = get_object_vars(json_decode($formValues['metadatas']));
            $entity = $formValues['entity'];
        }else{
            $sessionValues = $this->container->get('session')->get("filter_values", array());
            $this->container->get('session')->set("filter_values",$sessionValues);
            $metas = get_object_vars(json_decode($sessionValues['metadatas']));
            $entity = $sessionValues['entity'];
        }
        $render = get_object_vars($metas['render']);
        $this->render = $render['list'];
        //get entity metadatas to detect type of criterias
        $this->filtersBuilder->setEntityMetadatas($this->container->get('doctrine')->getManager()->getMetadataFactory()->getMetadataFor($entity));

        $query = $this->queryBuilder->searchQuery($this->container->get('session')->get("filter_values"));

        //paginate results
        $adapter = new DoctrineORMAdapter($query);
        $paginator = new Pagerfanta($adapter);
        $paginator->setMaxPerPage(10)->setCurrentPage($page);

        return array(
            "page"=>$page,
            "way"=>$way,
            "orderBy"=>$orderBy,
            "paginator"=>$paginator,
            );

    }


}
