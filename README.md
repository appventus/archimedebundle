ArchidedeBundle
========================

Archimede est un Repository amélioré. Il permet de construire des requetes, pagers et des filtres de recherche.

Il est articulé autours de 3 composants principaux :

1) QueryBuilder
------------------------

exemple)


Le query builder est le coeur d'archimede. Il permet de construire une query Doctrine à partir des arguments fournis.
Voici un exemple de syntaxe :


            $metadatas['joins']['planningUnits'] = "entity.planningUnits";
            $metadatas['joins']['ads'] = "entity.ads";
            $metadatas['joins']['applicants'] = "ads.applicants";
            $metadatas['adStatus']['type'] = 'customRelation';
            $metadatas['adStatus']['options'] = array(array("entity"=>"ads", "field"=>"status"));
            $metadatas['applicantStatus']['type'] = 'customRelation';
            $metadatas['applicantStatus']['options'] = array(array("entity"=>"applicants", "field"=>"status"));
            $metadatas['applicantRemoved']['type'] = 'customRelation';
            $metadatas['applicantRemoved']['options'] = array(array("entity"=>"applicants", "field"=>"cancelled"));

			$recruiterTaintedValues = array(
			            'entity' => 'JobBoardBundle:Recruiter',
			            'event' => array($event->getId()),
                        'cancelled' => array(0),
                        'adStatus' => array('published'),
                        'applicantStatus' => array('valid'),
                        'removed' => array(0),
                        'applicantRemoved' => array(0),
                        'metadatas' => json_encode($metadatas));

			$recruitersForEvent = $this->getContainer()->get('search_engine.query')->searchQuery($recruiterTaintedValues)->execute();

Dans l'exemple ci-dessus va faire un SELECT sur la table Recruiter, avec des jointures sur PlanningUnits, Ad et Applicant. Elle va appliquer des criteres sur les champs event, cancelled et removed de Recruiter ainsi que sur le champ status de Applicant et de Ad, et le champs removed de Applicant.

Les arguments du query builder sont constitués d'une part de "criterias" et de "metadatas". Les metadatas définissent les types de relations et les operateurs utilisés, et les criterias définissent les conditions de notre requete.

A) Entity

L'élement entity définit la table sur laquelle notre requete va executer le select.
Il prend la forme suivante:
			$values = array(
				[...],
				'entity' => 'MyBundle:MyEntity',
				[...]
			);

B) Criterias

Les criterias sont les champs sur lesquels une condition va s'appliquer. Ils servent à la partie WHERE de notre requete.
Ils se définissent de la sorte:
			$values = array(
				[...],
				'<critera>' => array(<criteriaValue>)
				[...]
			};

C) Metadatas

Les métadatas sont les arguments relatifs a la construction de la requete. On peux y définir les jointures et les champs externes sur lesquels appliquer nos criteres.
			$metadatas = array(<arguments>);
			$values = array(
				[...],
				'metadatas' => json_encode($metadatas),
				[...]
			};
Les arguments possibles sont définis ce-dessous:
a) Jointures

Les métadatas permettent d'ajouter des jointures de la façon suivante:

            $metadatas['joins']['<alias>'] = "<entity|alias>.<relation>";
La table jointe sera accessible via son alias.

b) CustomRelation

Cet type permet de fixer un critere sur un champs en particulier d'une table jointe.

            $metadatas['<alias>']['type'] = 'customRelation';
            $metadatas['<alias>']['options'] = array(array("entity"=>"<entity>", "field"=>"<fieldName>"));


2) PaginatorBuilder
---------------------

Le paginateur permet de construire un pager. Il prend en parametre un tableau de values identique au queryBuilder :

			$recruitersForEventPaginator = $this->getContainer()->get('search_engine.paginator')->create($values, $page, $displayResultsNb);


3) FilterBuilder
---------------------

Le filter builder prend est utilisable directement d'une vue vua un twig renderer. En lui passant le même type d'arguments qu'au query builder ainsi qu'un template de "filters" et un template "liste", il affiche un formulaire avec les champs sur lesquels vous avez choisi de filter.

					 {{ render(controller("AvArchimedeBundle:SearchEngine:filters", {
                        'entityName': 'MyBundle:Ad',
                        'criterias':
                            {
                                0:'trades',
                                1:
                                {
                                    'keyword':
                                        {
                                            0:'title',
                                            1:'description'
                                        }
                                },
                                2:'company'
                            },
                        'scope':
                            {
                                'status':
                                    {
                                        0:'published'
                                    }
                            },
                        'options':
                            {
                                'operandes':
                                    {
                                        'trades':'=',
                                        'company':'=',
                                        'keyword':{0:'LIKE', 1:'LIKE'},
                                        'status':'='

                                    },
                            },
                        'render':
                            {
                                'list':'MyJobBoardBundle:AdFront:list',
                                'filters':'MyJobBoardBundle:Front:filters'
                            }
                        }
                        ))
                    }}







